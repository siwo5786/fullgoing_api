package com.fullgoing.fullgoingapi.service.authCheck;

import com.fullgoing.fullgoingapi.entity.AuthCheck;
import com.fullgoing.fullgoingapi.entity.MessageSendHistory;
import com.fullgoing.fullgoingapi.enums.MessageTemplate;
import com.fullgoing.fullgoingapi.exception.CMissingAuthSuccessException;
import com.fullgoing.fullgoingapi.exception.CMissingDataException;
import com.fullgoing.fullgoingapi.exception.CWrongAuthNumberException;
import com.fullgoing.fullgoingapi.model.member.AuthCheckCodeResponse;
import com.fullgoing.fullgoingapi.model.member.MemberAuthRequest;
import com.fullgoing.fullgoingapi.repository.AuthCheckRepository;
import com.fullgoing.fullgoingapi.service.messageSendHistory.MessageSendHistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
public class AuthCheckService { // 인증과 관련된 서비스
    private final AuthCheckRepository authCheckRepository;
    private final MessageSendHistoryService messageSendHistoryService;

    public void setAuthCheck(MemberAuthRequest memberAuthRequest){ //인증 발송 메서드. 전화번호를 입력하면 해당 번호로 인증번호를 발송한다.
        Optional<AuthCheck> isExist = authCheckRepository.findByReceiveNum(memberAuthRequest.getPhoneNumber());
        // 인증을 확인할 때, 이 테이블에서 전화번호와 인증번호가 동일한지 체크할 예정이다. 만약 같은 전화번호를 가진 레코드가 한 개 이상이면, 찾기 불편할 뿐더러
        // 메세지 발송 내역은 이미 MessageSendHistory에서 관리하고 있으므로 남겨놓을 필요가 없다. 따라서,
        if (isExist.isPresent()) deleteCheckNum(memberAuthRequest.getPhoneNumber());
        // 만약 이미 동일한 전화번호가 테이블에 존재하면, 입력받은 전화번호에 해당 레코드를 삭제하는 메서드를 실행해준다.

        String randomNum = makeRandomNum(6); // 인증번호 발송용 6자리 난수 생성 메서드를 실행해준다.
        AuthCheck authCheck = new AuthCheck.Builder(memberAuthRequest.getPhoneNumber(), randomNum).build();
        // 전화번호와 생성된 난수를 인증 관리 빌더에 넘겨주고,

        authCheckRepository.save(authCheck);
        // repository에 그 데이터를 저장시킨다.

        HashMap<String, String> msgMap = new HashMap<>(); // 템플릿은 만들어져 있기 때문에, 특정 부분을 인증번호로 바꿔주면 된다.
        msgMap.put("인증번호", randomNum); // 템플릿의 "인증번호" 부분을 난수로 비꿔준 상태로
        messageSendHistoryService.sendMessage(MessageTemplate.TD_8763,"010-0000-0000",  memberAuthRequest.getPhoneNumber(), msgMap);
        // 메세지 발송 내역에 저장해준다.
    }
    public void authCheck(String phoneNum, String authNum){ //인증 확인 메서드.
        AuthCheck authCheck = authCheckRepository.findByReceiveNumOrderByIdDesc(phoneNum).orElseThrow(CMissingDataException::new);
        // 전화번호와 인증번호를 확인하여 일치하는지 확인해야한다. 근데 인증 관리 테이블에 입력 받은 전화번호에 해당하는 레코드가 없다면, exception처리 해준다.

        if (authNum.equals(authCheck.getAuthNum())){ // 일치하는 전화번호를 찾았다면, 인증 관리 테이블에 저장된 인증번호와 입력 받은 번호가 동일한지 확인하고,
            authCheck.putAuthCheckStatusSuccess(); // 만약 일치한다면 인증 상태를 성공으로 수정하고
            authCheckRepository.save(authCheck); // 그 데이터를 저장한다.

        } else throw new CWrongAuthNumberException(); // 만약 인증번호가 일치 하지 않으면 exception처리 한다.
    }
    public Boolean checkAuthStatus(String phoneNum){ // 인증 상태를 확인하는 메서드. 전화번호를 입력 받아서 해당 인증 상태를 return해주는 메서드이다.
        AuthCheck authCheck = authCheckRepository.findByReceiveNumOrderByIdDesc(phoneNum).orElseThrow(CMissingDataException::new);
        return authCheck.getAuthCheckStatus();
    }

    public void deleteCheckNum(String phoneNum){ // 테이블에 동일한 전화번호를 삭제하기 위한 메서드.
        AuthCheck authCheck = authCheckRepository.findByReceiveNum(phoneNum).orElseThrow(CMissingDataException::new);
        authCheckRepository.deleteById(authCheck.getId());
    }
    public String makeRandomNum(int x){ // 6자리 난수 생성 메서드.
        Random random = new Random();
        int createNum = 0;
        String randomNum = "";
        String resultNum = "";

        for (int i = 0; i < x; i++){
            createNum = random.nextInt(9);
            randomNum = Integer.toString(createNum);
            resultNum += randomNum;
        }
        return resultNum;
    }
}

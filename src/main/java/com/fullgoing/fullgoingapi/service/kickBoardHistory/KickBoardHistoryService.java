package com.fullgoing.fullgoingapi.service.kickBoardHistory;

import com.fullgoing.fullgoingapi.entity.AmountChargingHistory;
import com.fullgoing.fullgoingapi.entity.KickBoard;
import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.exception.*;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.KickBoardHistoryOwnHistoryItem;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.KickBoardHistoryOwnResponse;
import com.fullgoing.fullgoingapi.model.kickBoardHistory.MyKickBoardUsingResponse;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseEndRequest;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseStartRequest;
import com.fullgoing.fullgoingapi.repository.KickBoardHistoryRepository;
import com.fullgoing.fullgoingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class KickBoardHistoryService { // 킥보드 사용 관리 서비스
    private final KickBoardHistoryRepository kickBoardHistoryRepository;

    public void setKickBoardHistoryStart(Member member, KickBoard kickBoard, KickBoardUseStartRequest kickBoardUseStartRequest){
        // 킥보드 시작처리 메서드
        Optional<KickBoardHistory> historyCheckData = kickBoardHistoryRepository.findByMemberAndIsComplete(member, false);
        // 사용자가 이미 킥보드를 사용 중이라면 사용하지 못하게 해야하기 때문에 member와 종료 여부를 가져와 체크한다.

        if (!member.getIsLicence()) // 회원이 아직 면허 등록을 하지 않았으면 exception.
            throw new CIsLicenceFalseException();
        if (historyCheckData.isPresent()) // 사용자가 이미 킥보드를 이용 중이라면 exception.
            throw new CAlreadyUsingKickBoardException();
        if (kickBoard.getIsUse()) // 사용할 킥보드가 이미 사용 중이라면 exception.
            throw new CSomeoneUseThisKickBoardException();

        KickBoardHistory kickBoardHistory = new KickBoardHistory.Builder(member, kickBoard, kickBoardUseStartRequest).build();
        // 위에 해당하지 않는다면, 입력 받은 member와 kickboard와 위,경도를 빌더에게 넘긴다.
        kickBoardHistory.getKickBoard().putStatusUseStart();
        // 해당 킥보드는 사용 중으로 바꿔야 하므로 킥보드의 상태를 바꾸는 메서드를 호출한다.

        kickBoardHistoryRepository.save(kickBoardHistory);
        // 이를 repository에게 저장하게 시킨다.
    }

    public KickBoardHistory putKickBoardHistoryEnd(long kickBoardHistoryId, double passTime, KickBoardUseEndRequest kickBoardUseEndRequest){
        // 킥보드 사용 종료 메서드
        KickBoardHistory kickBoardHistory = kickBoardHistoryRepository.findById(kickBoardHistoryId).orElseThrow(CMissingDataException::new);
        // 킥보드 이용 내역 id를 받아와서 repository에게 찾아오게 시킨다. 해당 하는 데이터가 없다면 exception처리 한다.
        if (kickBoardHistory.getIsComplete()) throw new CAlreadyFinishUseHistoryException();
        // 무언가의 오류로 이미 종료 처리가 되었으나 인식하지 못 할 수도 있으므로, 해당 데이터가 종료처리 되었다면 exception처리 한다.

        kickBoardHistory.putUseEnd(kickBoardUseEndRequest);
        // 이용내역 테이블의 사용 종료용 메서드에 kickBoardUseEndRequest를 넘겨주고 호출한다.

        // 사용한 시간을 구하여 결제 금액을 구하는 로직을 짤 예정이다.
        LocalDateTime startTime = kickBoardHistory.getDateStart(); // 시작시간을 가져오고
        LocalDateTime endTime = kickBoardHistory.getDateEnd(); // 종료 시간을 가져와서
        long useSeconds = ChronoUnit.SECONDS.between(startTime,endTime); // 초 단위로 변환 하고
        double useMinutes = Math.ceil((double) useSeconds / 60); // 결제 패스 계산용 분을 변수에 담아놓는다.
        double totalMinutes = useMinutes - passTime; // 이용 시간은 사용한 시간 - 소지 패스 시간이 될 것이다.

        double resultPrice; // 결제 금액을 담아 놓을 변수를 하나 만들어 놓는다.

        if (totalMinutes > 0) { // 만약 totalMinutes가 양수라는 의미는 사용한 시간이 소지한 패스 시간보다 많다는 의미므로 분 당 이용 금액도 계산 해 줘야 한다.
           resultPrice = kickBoardHistory.getKickBoard().getPriceBasis().getBasePrice()
                   // 따라서 결제 금액은 킥보드 잠금해제 금액 + 분당 요금 * totalMinutes가 될 것이다.
                    + (kickBoardHistory.getKickBoard().getPriceBasis().getPerMinPrice() * Math.ceil(totalMinutes));
        } else { // 만약 0 혹은 음수라면 패스 시간이 이용 시간과 동일하거나 많다는 것이므로 분 당 요금은 계산할 필요가 없다.
            passTime = useMinutes; // 따라서 사용한 패스 시간은 이용 시간과 동일할 것이고,
            resultPrice = kickBoardHistory.getKickBoard().getPriceBasis().getBasePrice();
            // 결제 금액은 킥보드의 잠금 해제 비용만 나올 것이다.
        }
        kickBoardHistory.putResultPricePass(resultPrice, passTime);
        // 위에서 계산한 금액 및 패스 시간을 이용내역의 결제 금액과 소비 패스 시간, 사용 종료 여부를 수정하는 메서드에 담는다.
        kickBoardHistory.getKickBoard().putStatusUseEnd(kickBoardUseEndRequest);
        // 마지막으로, 완전히 사용히 종료 되었으니, 해당 킥보드의 상태를 종료로 변경하는 메서드에 kickBoardUseEndRequest에 담긴 위,경도를 넘겨주고 호출한다.

        return kickBoardHistoryRepository.save(kickBoardHistory);
        // 이곳에 담긴 정보를 잔여 금액 계산 및 소지 패스 계산 등에 사용 될 수 있으므로 return 시켜준다.
    }
    public ListResult<KickBoardHistoryOwnHistoryItem> getOwnUseHistory(Member member, int page){ //자신의 이용 내역을 불러오는 메서드.
        Page<KickBoardHistory> originList = kickBoardHistoryRepository.findAllByMemberOrderByIdDesc(member, ListConvertService.getPageable(page));
        // 자신의 토큰 값을 기준으로 값을 불러올 것이기 때문에 member를 받아온다. 값이 많으면 보기에 불편할 수도 있으니 페이지 형식으로 만들 것이다.

        List<KickBoardHistoryOwnHistoryItem> result = new LinkedList<>();
        // 사용자에게 보여줄 필드만 따로 뽑아서 Item으로 만들고 originList에서 하나씩 뽑아서 틀에 맞게 엮어줄 예정이다.

        originList.getContent().forEach(e->{
            KickBoardHistoryOwnHistoryItem item = new KickBoardHistoryOwnHistoryItem.Builder(e).build();
            // originList의 값(e)를 Item의 빌더에 넣어 주고,
            result.add(item); // 한 묶음으로 엮는다. 이 작업을 originList 안에 값이 다 떨어질 때까지 반복한다.
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    } // 페이지 형식으로 만들었으니 반환해줘야 할 값은, 묶어 놓은 result가 되고,
    // 총 몇 개의 결과가 나왔는지 보여줘야하기 때문에 originList의 총 갯수,
    // 총 몇 페이지가 나왔는지 보여줘야 하기 때문에 originList의 총 페이지(기본 값이 10이기 때문에 총 갯수 / 10)
    // 현재 몇 페이지인지 보여줘야 하기 때문에 originList의 입력 받은 페이지 값.

    public KickBoardHistoryOwnResponse getOwnUseSingleHistory(Member member, long historyId){ // 자신의 이용 내역 상세보기용 메서드.
        KickBoardHistory singleData = kickBoardHistoryRepository.findByMemberAndId(member, historyId).orElseThrow(CMissingDataException::new);
        // 마찬가지로 토큰 값 기준으로 불러올 것이다. 만약 해당하는 값이 없으면 exception 처리 해준다.
        return new KickBoardHistoryOwnResponse.Builder(singleData).build();
        // 사용자에게 보여질 값의 그릇인 KickBoardHistoryOwnResponse의 빌더에 받은 값을 넣어 return 시켜준다.
    }
    public MyKickBoardUsingResponse getMyUsingHistory(Member member){
        // 현재 자신이 사용 중인 이용 상태를 받아오는 메서드.
        Optional<KickBoardHistory> historyData = kickBoardHistoryRepository.findByMemberAndIsComplete(member, false);
        // 토큰 값과 종료 상태가 false인 데이터를 가져올 것이다. 로직 상 사용 시작에서 자신의 종료 상태가 false인 데이터가 있으면 시작할 수 없게 해 놓았기 때문에
        // 나오는 결과값은, 한 개 혹은 없을 것이다.

        if (historyData.isPresent()) {return new MyKickBoardUsingResponse.Builder(
                // 만약 있다면, 아래 값들을 보여줄 그릇인 MyKickBoardUsingResponse의 빌더에게 담는다.
                historyData.get().getId(), // 해당 이용 내역의 id
                historyData.get().getKickBoard().getModelName(), // 킥보드의 모델 명
                historyData.get().getStartPosX(), // 위도
                historyData.get().getStartPosY(), // 경도
                CommonFormat.convertLocalDateTimeToString(historyData.get().getDateStart()), // 사용 시작시간. 이 때 LocalDateTime을 그대로 표시하면
                // 나노 초까지 나오기 때문에 만들어 놓은 포맷에 맞춰 변형 시킨다.
                historyData.get().getKickBoard().getPriceBasis().getName(), // 킥보드의 가격 기준 코드 명
                historyData.get().getKickBoard().getPriceBasis().getBasePrice(), // 킥보드의 해제요금
                historyData.get().getKickBoard().getPriceBasis().getPerMinPrice() // 킥보드의 분당요금
        ).build();}
        // 이용 중인 내역이 없을 시, 아래와 같이 초기화 한다.
        else {return new MyKickBoardUsingResponse.Builder(
                0L,
                "",
                0D,
                0D,
                "",
                "",
                 0D,
                0D
        ).build();}
    }
}

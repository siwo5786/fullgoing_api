package com.fullgoing.fullgoingapi.service.comments;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import com.fullgoing.fullgoingapi.entity.Comments;
import com.fullgoing.fullgoingapi.exception.CMissingDataException;
import com.fullgoing.fullgoingapi.model.comments.CommentsItem;
import com.fullgoing.fullgoingapi.model.comments.CommentsRequest;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.repository.CommentsRepository;
import com.fullgoing.fullgoingapi.service.common.ListConvertService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CommentsService { // 댓글 관리용 서비스
    private final CommentsRepository commentsRepository;

    public void setComments(MessageBoard messageBoard, CommentsRequest commentsRequest){ // 댓글을 등록시키는 메서드
        Comments comments = new Comments.Builder(messageBoard, commentsRequest).build();
        // 댓글 테이블의 빌더에게 게시글 타입과 댓글 입력용 request를 넘겨주고

        commentsRepository.save(comments);
        // repository에게 저장시킨다.
    }
    public ListResult<CommentsItem> getCommentsByPages(int page, MessageBoard messageBoard){ // 게시글의 댓글을 보여주는 메서드
        Page<Comments> originList = commentsRepository.findAllByMessageBoardOrderByIdDesc(messageBoard ,ListConvertService.getPageable(page,20));
        // 볼 게시글에 해당하는 모든 데이터를 commentsRepository에게 찾아오도록 시킨다.
        // 이 값들이 많아서 한 페이지에 다 보이지 않을 수도 있으니 페이지 형식으로 만들어준다.

        List<CommentsItem> result = new LinkedList<>();
        // 사용자에게 보여줄 필드만 따로 뽑아서 Item으로 만들고 originList에서 하나씩 뽑아서 틀에 맞게 엮어줄 예정이다.

        originList.forEach(e->{
            CommentsItem commentsItem = new CommentsItem.Builder(e).build();
            // originList의 값(e)를 Item의 빌더에 넣어 주고,
            result.add(commentsItem); // 한 묶음으로 엮는다. 이 작업을 originList 안에 값이 다 떨어질 때까지 반복한다.
        });
        return ListConvertService.settingResult(result, originList.getTotalElements(), originList.getTotalPages(), originList.getPageable().getPageNumber());
    } // 페이지 형식으로 만들었으니 반환해줘야 할 값은, 묶어 놓은 result가 되고,
    // 총 몇 개의 결과가 나왔는지 보여줘야하기 때문에 originList의 총 갯수,
    // 총 몇 페이지가 나왔는지 보여줘야 하기 때문에 originList의 총 페이지(기본 값이 10이기 때문에 총 갯수 / 10)
    // 현재 몇 페이지인지 보여줘야 하기 때문에 originList의 입력 받은 페이지 값.

    public void putComment(long id, CommentsRequest commentsRequest){ // 댓글을 수정하는 메서드
        Comments comments = commentsRepository.findById(id).orElseThrow(CMissingDataException::new);
        // 해당 댓글의 id를 commentsRepository에게 찾아오도록 시킨다. 만약 없으면 exception처리 한다.
        comments.putComment(commentsRequest);
        // 가져온 데이터에 commentsRequest의 값을 넣고,

        commentsRepository.save(comments);
        // repository에게 저장하도록 시킨다.
    }
    public void deleteComment(long id){
        commentsRepository.deleteById(id);
    } // 댓글을 삭제하는 메서드.
    public void deleteCommentByContents(long historyId){ // 게시글이 삭제됬을 때, 게시글의 댓글까지 삭제시키는 메서드.
        List<Comments> comments = commentsRepository.findAllByMessageBoard_Id(historyId);
        // 입력 받은 게시글의 id를 Comments 테이블에서 전부 찾아온다.

        comments.forEach(e->{ // e 안에 담긴 값 = 입력 받은 게시글 id와 일치하는 모든 댓글 레코드.
            commentsRepository.deleteById(e.getId()); // respotitory에게 e의 id 값을 주고 삭제하도록 시킨다.
            // 이를 e에 더 이상 담긴 값이 없을 때 까지 반복 한다.
        });
    }

}

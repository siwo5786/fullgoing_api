package com.fullgoing.fullgoingapi.service.common;

import com.fullgoing.fullgoingapi.model.common.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService {
    public static PageRequest getPageable(int pageNum) {
        return PageRequest.of(pageNum - 1, 10);
    }
    // 페이지로 만들 때 필요한 메서드. pageNum은 표시할 페이지.

    public static PageRequest getPageable(int pageNum, int pageSize) {
        return PageRequest.of(pageNum - 1, pageSize);
    }
    // 페이지로 만들 때 필요한 메서드. pageNum은 표시할 페이지, pageSize는 한 페이지에 표시될 데이터 수.

    public static <T> ListResult<T> settingResult(List<T> list) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount((long)list.size());
        result.setTotalPage(1);
        result.setCurrentPage(1);

        return result;
    }

    public static <T> ListResult<T> settingResult(List<T> list, long totalItemCount, int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount(totalItemCount);
        result.setTotalPage(totalPage == 0 ? 1 : totalPage);
        result.setCurrentPage(currentPage + 1);

        return result;
    }
}

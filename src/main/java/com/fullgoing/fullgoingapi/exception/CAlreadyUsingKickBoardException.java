package com.fullgoing.fullgoingapi.exception;

public class CAlreadyUsingKickBoardException extends RuntimeException {
    public CAlreadyUsingKickBoardException(String msg, Throwable t) {
        super(msg, t);
    }

    public CAlreadyUsingKickBoardException(String msg) {
        super(msg);
    }

    public CAlreadyUsingKickBoardException() {
        super();
    }
}

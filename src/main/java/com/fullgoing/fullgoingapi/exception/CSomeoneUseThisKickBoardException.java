package com.fullgoing.fullgoingapi.exception;

public class CSomeoneUseThisKickBoardException extends RuntimeException {
    public CSomeoneUseThisKickBoardException(String msg, Throwable t) {
        super(msg, t);
    }

    public CSomeoneUseThisKickBoardException(String msg) {
        super(msg);
    }

    public CSomeoneUseThisKickBoardException() {
        super();
    }
}

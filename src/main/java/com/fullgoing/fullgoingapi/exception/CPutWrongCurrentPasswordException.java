package com.fullgoing.fullgoingapi.exception;

public class CPutWrongCurrentPasswordException extends RuntimeException {
    public CPutWrongCurrentPasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPutWrongCurrentPasswordException(String msg) {
        super(msg);
    }

    public CPutWrongCurrentPasswordException() {
        super();
    }
}

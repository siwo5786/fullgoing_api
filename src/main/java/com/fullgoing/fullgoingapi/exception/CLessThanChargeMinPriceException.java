package com.fullgoing.fullgoingapi.exception;

public class CLessThanChargeMinPriceException extends RuntimeException {
    public CLessThanChargeMinPriceException(String msg, Throwable t) {
        super(msg, t);
    }

    public CLessThanChargeMinPriceException(String msg) {
        super(msg);
    }

    public CLessThanChargeMinPriceException() {
        super();
    }
}

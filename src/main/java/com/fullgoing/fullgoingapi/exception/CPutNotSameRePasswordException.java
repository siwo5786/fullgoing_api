package com.fullgoing.fullgoingapi.exception;

public class CPutNotSameRePasswordException extends RuntimeException {
    public CPutNotSameRePasswordException(String msg, Throwable t) {
        super(msg, t);
    }

    public CPutNotSameRePasswordException(String msg) {
        super(msg);
    }

    public CPutNotSameRePasswordException() {
        super();
    }
}

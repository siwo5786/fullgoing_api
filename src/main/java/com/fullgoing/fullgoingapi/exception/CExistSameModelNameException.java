package com.fullgoing.fullgoingapi.exception;

public class CExistSameModelNameException extends RuntimeException {
    public CExistSameModelNameException(String msg, Throwable t) {
        super(msg, t);
    }

    public CExistSameModelNameException(String msg) {
        super(msg);
    }

    public CExistSameModelNameException() {
        super();
    }
}

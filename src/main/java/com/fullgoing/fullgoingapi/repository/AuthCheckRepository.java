package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.AuthCheck;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.swing.text.html.Option;
import java.util.Optional;

public interface AuthCheckRepository extends JpaRepository<AuthCheck, Long> {

    Optional<AuthCheck> findByReceiveNumOrderByIdDesc(String phoneNum);
    // 입력된 전화번호와 전송된 인증번호가 일치하는지 확인용.

    Optional<AuthCheck> findByReceiveNum(String phoneNum);
    // 입력된 전화번호로 메세지 전송용.
    // 회원가입이 완료된 후, 인증내역 지우기용.


}

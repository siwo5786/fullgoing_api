package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.MessageBoard;
import com.fullgoing.fullgoingapi.entity.Comments;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CommentsRepository extends JpaRepository<Comments, Long> {
    Page<Comments> findAllByMessageBoardOrderByIdDesc(MessageBoard messageBoard, Pageable pageable);
    // 게시글의 댓글 찾아오기용

    List<Comments> findAllByMessageBoard_Id(long historyId);
    //게시글이 지워질 때, 하위 댓글까지 전부 지우기용.
}

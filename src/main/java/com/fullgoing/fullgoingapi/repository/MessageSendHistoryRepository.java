package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.MessageSendHistory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageSendHistoryRepository extends JpaRepository<MessageSendHistory, Long> {
}

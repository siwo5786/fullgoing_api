package com.fullgoing.fullgoingapi.repository;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RemainingAmountRepository extends JpaRepository<RemainingAmount, Long> {
    Optional<RemainingAmount> findByMember(Member member);

    Page<RemainingAmount> findAllByOrderByIdAsc(Pageable pageable);
}

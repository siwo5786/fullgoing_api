package com.fullgoing.fullgoingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor

public enum PassType {

    MIN_60("60분 패스", 60D, 6000D),
    MIN_120("120분 패스", 120D, 10000D);

    private final String name;
    private final Double min;
    private final Double price;
}

package com.fullgoing.fullgoingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ChargeType {
    MILEAGE("마일리지"),
    PRICE("금액"),
    PASS("패스");
    private final String name;
}

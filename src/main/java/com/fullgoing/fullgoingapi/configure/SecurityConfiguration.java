package com.fullgoing.fullgoingapi.configure;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.List;

@Configuration
@RequiredArgsConstructor
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    private final JwtTokenProvider jwtTokenProvider;

    private static final String[] AUTH_WHITELIST = {
            "/swagger-resources/**",
            "/swagger-ui.html",
            "/v2/api-docs",
            "/webjars/**"
    };

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(AUTH_WHITELIST);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManagerBean();
    }

    /*
    flutter 에서 header에 token 넣는법
    String? token = await TokenLib.getToken();

    Dio dio = Dio();
    dio.options.headers['Authorization'] = 'Bearer ' + ${token!}; <-- 한줄 추가

    --- 아래 동일
     */

    // 퍼미션이 무엇인지 보기.
    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .httpBasic().disable()
                .cors().configurationSource(corsConfigurationSource())
                .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .authorizeRequests()
                        .antMatchers(HttpMethod.GET, "/exception/**").permitAll() // 전체 허용

                        //잔액 관리 컨트롤러
                        .antMatchers("/v1/remaining-amount/my/**").hasAnyRole("USER") //패스, 금액 충전
                        .antMatchers("/v1/remaining-amount/plus/**").hasAnyRole("ADMIN") //패스, 금액 충전

                        //회원정보 컨트롤러
                        .antMatchers("/v1/member-info/username/duplicate/check").permitAll() //아이디 중복확인
                        .antMatchers("/v1/member-info/my/profile").hasAnyRole("USER","ADMIN") //프로필
                        .antMatchers("/v1/member-info/licence").hasAnyRole("USER","ADMIN") //면허증 등록
                        .antMatchers("/v1/member-info/change-password").hasAnyRole("USER","ADMIN") // 비밀번호 변경
                        .antMatchers("/v1/member-info/find-password").permitAll() //비밀번호 찾기
                        .antMatchers("/v1/member-info/all").hasAnyRole("ADMIN") //회원정보 가져오기
                        .antMatchers("/v1/member-info/detail/member-id/{memberId}").hasAnyRole("ADMIN") //회원정보 가져오기


                        //회원등록 컨트롤러
                        .antMatchers("/v1/member/join").permitAll() //회원가입
                        .antMatchers("/v1/member/data").hasAnyRole("ADMIN") //회원 등록

                        //로그인 컨트롤러
                        .antMatchers("/v1/member/login/**").permitAll() //로그인

                        //킥보드 이용내역 컨트롤러
                        .antMatchers("/v1/kick-board-history/start-use/**").hasAnyRole("USER","ADMIN") //킥보드 사용 시작
                        .antMatchers("/v1/kick-board-history/end-use/**").hasAnyRole("USER","ADMIN") //킥보드 사용 종료
                        .antMatchers("/v1/kick-board-history/my/**").hasAnyRole("USER","ADMIN") // 본인 이용내역 조회

                        //킥보드 컨트롤러
                        .antMatchers("/v1/kick-board/data").hasAnyRole("ADMIN") //킥보드 단일등록
                        .antMatchers("/v1/kick-board/near").hasAnyRole("USER","ADMIN") //주변 킥보드 찾기
                        .antMatchers("/v1/kick-board/file-upload").hasAnyRole("ADMIN") //킥보드 csv 파일 등록
                        .antMatchers("/v1/kick-board/all").hasAnyRole("ADMIN") //모든 킥보드 리스트
                        .antMatchers("/v1/kick-board/name-price-pos-memo-use/{kickBoardId}").hasAnyRole("ADMIN") //킥보드 정보 수정
                        .antMatchers("/v1/kick-board/info/kick-board-id/{kickBoardId}").hasAnyRole("ADMIN","USER") //킥보드 단일 정보 조회

                        //인증확인 컨트롤러
                        .antMatchers("/v1/auth/**").permitAll() //인증

                        //충전내역 컨트롤러
                        .antMatchers("/v1/charging-history/my/charges").hasAnyRole("USER","ADMIN") // 본인 충전내역 조회
                        .antMatchers("/v1/charging-history/charges").hasAnyRole("ADMIN") // 모든 충전내역 조회
                        .antMatchers("/v1/charging-history/charges/userName").hasAnyRole("ADMIN") // username으로 충전내역 조회

                        //결제 내역 컨트롤러
                        .antMatchers("/v1/charging-history/histories").hasAnyRole("ADMIN") // 매출 내역 조회


                        //게시판 컨트롤러
                        .antMatchers("/v1/message-board/data").hasAnyRole("USER","ADMIN") // 게시글 작성
                        .antMatchers("/v1/message-board/list").hasAnyRole("USER","ADMIN") // 게시글 리스트
                        .antMatchers("/v1/message-board/detail/history-id/{historyId}").hasAnyRole("USER","ADMIN") // 게시글 일람
                        .antMatchers("/v1/message-board/title-name-context-img/history-id/{historyId}").hasAnyRole("USER","ADMIN") // 게시글 수정
                        .antMatchers("/v1/message-board/history-id/{historyId}").hasAnyRole("USER","ADMIN") // 게시글 삭제

                        //댓글 컨트롤러
                        .antMatchers(HttpMethod.POST,"/v1/comment/history-id/{boardHistoryId}").hasAnyRole("USER","ADMIN") // 댓글 작성
                        .antMatchers(HttpMethod.GET,"/v1/comment/list/history-id/{historyId}").hasAnyRole("USER","ADMIN") // 댓글 리스트
                        .antMatchers(HttpMethod.PUT,"/v1/comment/comment-id/{commentId}").hasAnyRole("USER","ADMIN") // 댓글 수정
                        .antMatchers(HttpMethod.DELETE,"/v1/comment/comment-id/{commentId}").hasAnyRole("USER","ADMIN") // 댓글 삭제



                        .antMatchers("/v1/auth-test/test-admin").hasAnyRole("ADMIN")
                        .antMatchers("/v1/auth-test/test-user").hasAnyRole("USER")
                        .antMatchers("/v1/auth-test/test-all").hasAnyRole("ADMIN", "USER")
                        .antMatchers("/v1/auth-test/login-all/**").hasAnyRole("ADMIN", "USER")

                        .anyRequest().hasRole("ADMIN") // 기본 접근 권한은 ROLE_ADMIN
                .and()
                    .exceptionHandling().accessDeniedHandler(new CustomAccessDeniedHandler())
                .and()
                    .exceptionHandling().authenticationEntryPoint(new CustomAuthenticationEntryPoint())
                .and()
                    .addFilterBefore(new JwtTokenFilter(jwtTokenProvider), UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();

        configuration.addAllowedOriginPattern("*");
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.setAllowCredentials(true);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }


}

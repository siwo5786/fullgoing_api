package com.fullgoing.fullgoingapi.entity;

import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardNamePriceXYMemoIsUseUpdateRequest;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardRequest;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardUseEndRequest;
import com.fullgoing.fullgoingapi.model.kickboard.KickBoardsRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class KickBoard {
    // 시퀀스
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    // 킥보드 상태
    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private KickBoardStatus kickBoardStatus;

    // 모델명
    @Column(nullable = false, length = 20, unique = true)
    private String modelName;

    // 기준요금
    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private PriceBasis priceBasis;

    // 구입일
    @Column(nullable = false)
    private LocalDate dateBuy;

    // 위도
    @Column(nullable = false)
    private Double posX;

    // 경도
    @Column(nullable = false)
    private Double posY;

    // 비고
    @Column(columnDefinition = "TEXT")
    private String memo;

    // 사용여부
    @Column(nullable = false)
    private Boolean isUse;

    public void putStatusUseEnd(KickBoardUseEndRequest kickBoardUseEndRequest){
        this.posX = kickBoardUseEndRequest.getEndPosX();
        this.posY = kickBoardUseEndRequest.getEndPosY();
        this.kickBoardStatus = KickBoardStatus.READY;
        this.isUse = false;
    }

    public void putStatusUseStart(){
        this.kickBoardStatus = KickBoardStatus.ING;
        this.isUse = true;
    }

    public void putNamePricePosXYMemoIsUse(KickBoardStatus kickBoardStatus, KickBoardNamePriceXYMemoIsUseUpdateRequest updateRequest){
        this.modelName = updateRequest.getModelName();
        this.priceBasis = updateRequest.getPriceBasis();
        this.posX = updateRequest.getPosX();
        this.posY = updateRequest.getPosY();
        this.memo = updateRequest.getMemo();
        this.kickBoardStatus = kickBoardStatus;
    }

    public void putStatus(KickBoardStatus kickBoardStatus) {
        this.kickBoardStatus = kickBoardStatus;
    }

    public void putMemo(String memo) {
        this.memo = memo;
    }

    private KickBoard(csvBuilder builder) {
        this.kickBoardStatus = builder.kickBoardStatus;
        this.modelName = builder.modelName;
        this.priceBasis = builder.priceBasis;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.isUse = builder.isUse;
    }
    private KickBoard(Builder builder) {
        this.kickBoardStatus = builder.kickBoardStatus;
        this.modelName = builder.modelName;
        this.priceBasis = builder.priceBasis;
        this.dateBuy = builder.dateBuy;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.isUse = builder.isUse;
    }

    public static class csvBuilder implements CommonModelBuilder<KickBoard> {
        private final KickBoardStatus kickBoardStatus;
        private final String modelName;
        private final PriceBasis priceBasis;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;
        private final Boolean isUse;

        public csvBuilder(KickBoardsRequest request) {
            this.kickBoardStatus = KickBoardStatus.READY;
            this.modelName = request.getModelName();
            this.priceBasis = request.getPriceBasis();
            this.dateBuy = request.getDateBuy();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
            this.isUse = false;
        }

        @Override
        public KickBoard build() {
            return new KickBoard(this);
        }
    }
    public static class Builder implements CommonModelBuilder<KickBoard> {
        private final KickBoardStatus kickBoardStatus;
        private final String modelName;
        private final PriceBasis priceBasis;
        private final LocalDate dateBuy;
        private final Double posX;
        private final Double posY;
        private final Boolean isUse;

        public Builder(KickBoardRequest request) {
            this.kickBoardStatus = KickBoardStatus.READY;
            this.modelName = request.getModelName();
            this.priceBasis = request.getPriceBasis();
            this.dateBuy = request.getDateBuy();
            this.posX = request.getPosX();
            this.posY = request.getPosY();
            this.isUse = false;
        }

        @Override
        public KickBoard build() {
            return new KickBoard(this);
        }
    }

}

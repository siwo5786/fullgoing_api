package com.fullgoing.fullgoingapi.entity;



import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import com.fullgoing.fullgoingapi.model.comments.CommentsRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Comments {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "messageBoardId", nullable = false)
    private MessageBoard messageBoard;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 100)
    private String userComment;

    @Column(nullable = false)
    private LocalDateTime wroteCommentDate;

    public void putComment(CommentsRequest commentsRequest){
        this.userComment = commentsRequest.getUserComments();
    }

    private Comments(Builder builder){
        this.messageBoard = builder.messageBoard;
        this.name = builder.name;
        this.userComment = builder.userComment;
        this.wroteCommentDate = builder.wroteCommentDate;
    }
    public static class Builder implements CommonModelBuilder<Comments>{

        private final MessageBoard messageBoard;
        private final String name;
        private final String userComment;
        private final LocalDateTime wroteCommentDate;

        public Builder(MessageBoard messageBoard, CommentsRequest commentsRequest){
            this.messageBoard = messageBoard;
            this.name = commentsRequest.getName();
            this.userComment = commentsRequest.getUserComments();
            this.wroteCommentDate = LocalDateTime.now();
        }

        @Override
        public Comments build() {
            return new Comments(this);
        }
    }
}

package com.fullgoing.fullgoingapi.model.member;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "고객 정보용 리스폰스")
public class MemberInfoResponse {

    @ApiModelProperty(notes = "아이디")
    private String username;

    @ApiModelProperty(notes = "고객 명")
    private String name;

    @ApiModelProperty(notes = "가입일")
    private String dateCreate;

    @ApiModelProperty(notes = "전화번호")
    private String phoneNumber;

    @ApiModelProperty(notes = "면허 소지 여부")
    private Boolean isLicence;

    @ApiModelProperty(notes = "잔여 마일리지")
    private BigDecimal remainMileage;

    @ApiModelProperty(notes = "잔여 패스 시간")
    private BigDecimal remainPass;

    @ApiModelProperty(notes = "잔여 금액")
    private BigDecimal remainPrice;

    private MemberInfoResponse(Builder builder){
        this.username = builder.username;
        this.name = builder.name;
        this.dateCreate = builder.dateCreate;
        this.phoneNumber = builder.phoneNumber;
        this.isLicence = builder.isLicence;
        this.remainMileage = builder.remainMileage;
        this.remainPass = builder.remainPass;
        this.remainPrice = builder.remainPrice;
    }
    public static class Builder implements CommonModelBuilder<MemberInfoResponse>{

        private final String username;
        private final String name;
        private final String dateCreate;
        private final String phoneNumber;
        private final Boolean isLicence;
        private final BigDecimal remainMileage;
        private final BigDecimal remainPass;
        private final BigDecimal remainPrice;

        public Builder(Member member, RemainingAmount remainingAmount){
            this.username = member.getUsername();
            this.name = member.getName();
            this.dateCreate = CommonFormat.convertLocalDateTimeToString(member.getDateCreate());
            this.phoneNumber = member.getPhoneNumber();
            this.isLicence = member.getIsLicence();
            this.remainMileage = CommonFormat.convertDoubleToDecimal(remainingAmount.getRemainMileage());
            this.remainPass = CommonFormat.convertDoubleToDecimal(remainingAmount.getRemainPass());
            this.remainPrice = CommonFormat.convertDoubleToDecimal(remainingAmount.getRemainPrice());
        }

        @Override
        public MemberInfoResponse build() {
            return new MemberInfoResponse(this);
        }
    }
}

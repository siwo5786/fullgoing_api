package com.fullgoing.fullgoingapi.model.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "비밀번호 찾기용 리퀘스트")
public class FindPasswordRequest {

    @ApiModelProperty(notes = "아이디 / 5 ~ 30자리", required = true)
    @NotNull
    @Length(min = 5, max = 30)
    private String username;

    @ApiModelProperty(notes = "연락처 / 13자리", required = true)
    @NotNull
    @Length(min = 13, max = 13)
    private String phoneNumber;
}

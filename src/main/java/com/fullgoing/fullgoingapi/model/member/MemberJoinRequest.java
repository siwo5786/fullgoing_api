package com.fullgoing.fullgoingapi.model.member;

import com.fullgoing.fullgoingapi.enums.MemberGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "관리자용 회원 등록 리퀘스트/회원가입용 리퀘스트 상속")
public class MemberJoinRequest extends MemberCreateRequest {
    @NotNull
    @Enumerated(EnumType.STRING)
    @ApiModelProperty(notes = "멤버 직위", required = true)
    private MemberGroup memberGroup;

    @ApiModelProperty(notes = "면허 번호")
    private String licenceNumber;
}

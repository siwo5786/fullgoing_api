package com.fullgoing.fullgoingapi.model.member;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "비밀번호 변경용 리퀘스트")
public class MemberPasswordPhoneNumUpdateRequest {

    @ApiModelProperty(notes = "현재 비밀번호 / 8 ~ 20자리", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String currentPassword;

    @ApiModelProperty(notes = "새로운 비밀번호 / 8 ~ 20자리", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String newPassword;

    @ApiModelProperty(notes = "비밀번호 확인 / 8 ~ 20자리", required = true)
    @NotNull
    @Length(min = 8, max = 20)
    private String newRePassword;

}

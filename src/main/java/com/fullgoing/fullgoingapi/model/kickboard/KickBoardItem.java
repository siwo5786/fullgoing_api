package com.fullgoing.fullgoingapi.model.kickboard;

import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "킥보드 리스트 보여주기용 아이템")
public class KickBoardItem {

    @ApiModelProperty(notes = "킥보드 상태")
    private String kickBoardStatusName;

    @ApiModelProperty(notes = "모델 명")
    private String modelName;

    @ApiModelProperty(notes = "가격기준코드")
    private String priceBasisName;

    @ApiModelProperty(notes = "위도")
    private Double posX;

    @ApiModelProperty(notes = "경도")
    private Double posY;

    @ApiModelProperty(notes = "거리")
    private Double distanceM;

    private KickBoardItem(Builder builder) {
        this.kickBoardStatusName = builder.kickBoardStatusName;
        this.modelName = builder.modelName;
        this.priceBasisName = builder.priceBasisName;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.distanceM = builder.distanceM;
    }

    public static class Builder implements CommonModelBuilder<KickBoardItem> {
        private final String kickBoardStatusName;
        private final String modelName;
        private final String priceBasisName;
        private final Double posX;
        private final Double posY;
        private final Double distanceM;

        public Builder(KickBoardStatus kickBoardStatus, String modelName, PriceBasis priceBasis, double posX, double posY, double distanceM) {
            this.kickBoardStatusName = kickBoardStatus.getName();
            this.modelName = modelName;
            this.priceBasisName = priceBasis.getName();
            this.posX = posX;
            this.posY = posY;
            this.distanceM = distanceM;
        }

        @Override
        public KickBoardItem build() {
            return new KickBoardItem(this);
        }
    }
}

package com.fullgoing.fullgoingapi.model.kickboard;

import com.fullgoing.fullgoingapi.entity.KickBoard;
import com.fullgoing.fullgoingapi.enums.PriceBasis;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "킥보드 리스트 페이지용 아이템")
public class KickBoardPageItem {

    @ApiModelProperty(notes = "킥보드 시퀀스")
    private Long kickBoardId;

    @ApiModelProperty(notes = "모델 명")
    private String modelName;

    @ApiModelProperty(notes = "위도")
    private Double posX;

    @ApiModelProperty(notes = "경도")
    private Double posY;

    @ApiModelProperty(notes = "킥보드 상태")
    private String kickBoardStatusName;

    private KickBoardPageItem(Builder builder){
        this.kickBoardId = builder.kickBoardId;
        this.modelName = builder.modelName;
        this.posX = builder.posX;
        this.posY = builder.posY;
        this.kickBoardStatusName = builder.kickBoardStatusName;
    }
    public static class Builder implements CommonModelBuilder<KickBoardPageItem>{

        private final Long kickBoardId;
        private final String modelName;
        private final Double posX;
        private final Double posY;
        private final String kickBoardStatusName;

        public Builder(KickBoard kickBoard){
            this.kickBoardId = kickBoard.getId();
            this.modelName = kickBoard.getModelName();
            this.posX = kickBoard.getPosX();
            this.posY = kickBoard.getPosY();
            this.kickBoardStatusName = kickBoard.getKickBoardStatus().getName();
        }

        @Override
        public KickBoardPageItem build() {
            return new KickBoardPageItem(this);
        }
    }
}

package com.fullgoing.fullgoingapi.model.kickboard;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
@ApiModel(description = "킥보드 사용 시작용 리퀘스트")
public class KickBoardUseStartRequest {
    @ApiModelProperty(notes = "시작 위도 / 음수 허용", required = true)
    @NotNull
    private Double startPosX;

    @ApiModelProperty(notes = "시작 경도 / 음수 허용", required = true)
    @NotNull
    private Double startPosY;

}

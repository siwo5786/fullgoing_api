package com.fullgoing.fullgoingapi.model.messageBoard;

import com.fullgoing.fullgoingapi.enums.ContentTheme;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "게시글 수정용 리퀘스트")
public class MessageBoardTitleNameImgUpdateRequest {

    @ApiModelProperty(name = "작성자 명", required = true)
    @NotNull
    @Length(min = 1, max = 50)
    private String name;

    @ApiModelProperty(name = "제목", required = true)
    @NotNull
    @Length(min = 1, max = 50)
    private String title;

    @ApiModelProperty(name = "본문", required = true)
    @NotNull
    private String context;

    @ApiModelProperty(name = "이미지 주소")
    private String imgName;
}

package com.fullgoing.fullgoingapi.model.remainingAmount;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ApiModel(description = "금액 충전용 리퀘스트")
public class RemainingAmountChargePriceRequest {

    @ApiModelProperty(notes = "충전할 금액 / 1000 ~ 1000000", required = true)
    @NotNull
    @Min(1000)
    @Max(1000000)
    private Double price;

}

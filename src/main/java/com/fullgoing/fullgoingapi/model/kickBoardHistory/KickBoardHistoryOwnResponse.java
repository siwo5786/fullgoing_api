package com.fullgoing.fullgoingapi.model.kickBoardHistory;

import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "자신용 킥보드 사용내역 보여주기용 아이템")
public class KickBoardHistoryOwnResponse {

    @ApiModelProperty(notes = "킥보드 명")
    private String kickBoardName;

    @ApiModelProperty(notes = "사용 시작일")
    private String dateStart;

    @ApiModelProperty(notes = "사용 종료일")
    private String dateEnd;

    @ApiModelProperty(notes = "총 가격")
    private BigDecimal resultPrice;

    @ApiModelProperty(notes = "소비 패스 시간")
    private BigDecimal resultPass;

    private KickBoardHistoryOwnResponse(Builder builder){
        this.kickBoardName = builder.kickBoardName;
        this.dateStart = builder.dateStart;
        this.dateEnd = builder.dateEnd;
        this.resultPrice = builder.resultPrice;
        this.resultPass = builder.resultPass;
    }
    public static class Builder implements CommonModelBuilder<KickBoardHistoryOwnResponse>{

        private final String kickBoardName;
        private final String dateStart;
        private final String dateEnd;
        private final BigDecimal resultPrice;
        private final BigDecimal resultPass;

        public Builder(KickBoardHistory kickBoardHistory){
            this.kickBoardName = kickBoardHistory.getKickBoard().getModelName();
            this.dateStart = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateStart());
            this.dateEnd = CommonFormat.convertLocalDateTimeToString(kickBoardHistory.getDateEnd());
            this.resultPrice = CommonFormat.convertDoubleToDecimal(kickBoardHistory.getResultPrice());
            this.resultPass = CommonFormat.convertDoubleToDecimal(kickBoardHistory.getResultPass());
        }

        @Override
        public KickBoardHistoryOwnResponse build() {
            return new KickBoardHistoryOwnResponse(this);
        }
    }
}

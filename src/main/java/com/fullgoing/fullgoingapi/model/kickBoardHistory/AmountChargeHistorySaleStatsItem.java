package com.fullgoing.fullgoingapi.model.kickBoardHistory;

import com.fullgoing.fullgoingapi.entity.AmountChargingHistory;
import com.fullgoing.fullgoingapi.entity.KickBoardHistory;
import com.fullgoing.fullgoingapi.interfaces.CommonModelBuilder;
import com.fullgoing.fullgoingapi.lib.CommonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@ApiModel(description = "관리자용 결제내역 리스트 보여주기 아이템")
public class AmountChargeHistorySaleStatsItem {


    @ApiModelProperty(notes = "날짜")
    private String saleDate;

    @ApiModelProperty(notes = "가격")
    private Double salePrice;

    private AmountChargeHistorySaleStatsItem(Builder builder){
        this.saleDate = builder.saleDate;
        this.salePrice = builder.salePrice;
    }
    public static class Builder implements CommonModelBuilder<AmountChargeHistorySaleStatsItem>{

        private final String saleDate;
        private final Double salePrice;

        public Builder(AmountChargingHistory amountChargingHistory){
            this.saleDate = CommonFormat.convertLocalDateTimeToString(amountChargingHistory.getDateReg());
            this.salePrice = amountChargingHistory.getPaymentAmount();
        }
        @Override
        public AmountChargeHistorySaleStatsItem build() {
            return new AmountChargeHistorySaleStatsItem(this);
        }
    }
}

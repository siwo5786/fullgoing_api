package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.common.SingleResult;
import com.fullgoing.fullgoingapi.model.member.*;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.MemberDataService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import com.fullgoing.fullgoingapi.service.member.MemberInfoService;
import com.fullgoing.fullgoingapi.service.remainingAmount.RemainingAmountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 정보")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member-info")
public class MemberInfoController {
    private final MemberInfoService memberInfoService;
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "회원 개인 잔액 및 정보 확인")
    @GetMapping("/my/profile")
    public SingleResult<MemberInfoResponse> getMemberSingleData(){
        Member member = profileService.getMemberData();

        return ResponseService.getSingleResult(memberInfoService.getMemberData(member));
    }
    @ApiOperation(value = "회원 리스트 확인 /관리자")
    @GetMapping("/all")
    public ListResult<MemberInfoItem> getMemberListByPage(@RequestParam(value = "page",required = false,defaultValue = "1") int page){
        return ResponseService.getListResult(memberInfoService.getMemberListByPage(page), true);
    }
    @ApiOperation(value = "회원 잔액 및 정보 확인 /관리자")
    @GetMapping("/detail/member-id/{memberId}")
    public SingleResult<MemberInfoResponse> getMemberData(@PathVariable long memberId){
        Member member = memberDataService.getMemberById(memberId);
        return ResponseService.getSingleResult(memberInfoService.getMemberData(member));
    }

    @ApiOperation(value = "면허증 등록 /회원 및 관리자")
    @PutMapping("/licence")
    public CommonResult putLicenceNum(@RequestBody @Valid MemberLicenceNumUpdateRequest updateRequest){
        Member member = profileService.getMemberData();
        memberDataService.setLicenceNum(member, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "비밀번호 변경 /회원 및 관리자")
    @PutMapping("/change-password")
    public CommonResult putPasswordPhoneNum(@RequestBody @Valid MemberPasswordPhoneNumUpdateRequest updateRequest){
        Member member = profileService.getMemberData();
        memberDataService.putPhoneNumPassword(member, updateRequest);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "비밀번호 찾기 /회원 및 관리자")
    @PutMapping("/find-password")
    public CommonResult findPassword(@RequestBody @Valid FindPasswordRequest findPasswordRequest){
        memberDataService.findPassword(findPasswordRequest);

        return ResponseService.getSuccessResult();
    }

}

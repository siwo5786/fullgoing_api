package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardItem;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardRequest;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardResponse;
import com.fullgoing.fullgoingapi.model.messageBoard.MessageBoardTitleNameImgUpdateRequest;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.common.SingleResult;
import com.fullgoing.fullgoingapi.service.messageBoard.MessageBoardService;
import com.fullgoing.fullgoingapi.service.comments.CommentsService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "게시판 정보")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/message-board")

public class MessageBoardController {
    private final MessageBoardService messageBoardService;
    private final CommentsService commentsService;

    @ApiOperation(value = "게시글 등록 /관리자")
    @PostMapping("/data")
    public CommonResult setBoardHistory(@RequestBody @Valid MessageBoardRequest messageBoardRequest){
        messageBoardService.setBoardHistory(messageBoardRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 페이지 리스트 /관리자")
    @GetMapping("/list")
    public ListResult<MessageBoardItem> getBoardHistoryByPage(@RequestParam(value = "page", required = false, defaultValue = "1") int page){
        return ResponseService.getListResult(messageBoardService.getBoardHistoriesByPage(page), true);
    }

    @ApiOperation(value = "게시글 일람 /관리자")
    @GetMapping("/detail/history-id/{historyId}")
    public SingleResult<MessageBoardResponse> getBoardHistory(@PathVariable long historyId){
        return ResponseService.getSingleResult(messageBoardService.getBoardHistory(historyId));
    }

    @ApiOperation(value = "게시글 수정 /관리자")
    @PutMapping("/title-name-context-img/history-id/{historyId}")
    public CommonResult putTitleThemeContext(@PathVariable long historyId, @RequestBody @Valid MessageBoardTitleNameImgUpdateRequest updateRequest){
        messageBoardService.putTitleThemeContext(historyId, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "게시글 삭제 /관리자")
    @DeleteMapping("/history-id/{historyId}")
    public CommonResult deleteContent(@PathVariable long historyId){
        commentsService.deleteCommentByContents(historyId);
        messageBoardService.deleteContent(historyId);
        return ResponseService.getSuccessResult();
    }
}

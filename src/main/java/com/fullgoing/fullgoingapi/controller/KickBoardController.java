package com.fullgoing.fullgoingapi.controller;


import com.fullgoing.fullgoingapi.enums.KickBoardStatus;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.common.ListResult;
import com.fullgoing.fullgoingapi.model.common.SingleResult;
import com.fullgoing.fullgoingapi.model.kickboard.*;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.kickBoard.KickBoardService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;

@Api(tags = "킥보드 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/kick-board")
public class KickBoardController {
    private final KickBoardService kickBoardService;

    @ApiOperation(value = "킥보드 단일 데이터 등록 /관리자")
    @PostMapping("/data")
    public CommonResult setKickBoard(@RequestBody @Valid KickBoardRequest kickBoardRequest){
        kickBoardService.setKickBoard(kickBoardRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "csv파일로 킥보드 업로드 /관리자")
    @PostMapping("/file-upload")
    public CommonResult setKickBoards(@RequestParam("csvFile") MultipartFile csvFile) throws Exception {
        kickBoardService.setKickBoards(csvFile);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "근처 킥보드 리스트 /회원 및 관리자")
    @GetMapping("/near")
    public ListResult<KickBoardItem> getNearList(@RequestParam("posX") double posX, @RequestParam("posY") double posY, @RequestParam("distanceKm") int distanceKm) {
        return ResponseService.getListResult(kickBoardService.getNearList(posX, posY, distanceKm), true);
    }

    @ApiOperation(value = "모든 킥보드 리스트 /관리자")
    @GetMapping("/all")
    public ListResult<KickBoardPageItem> getKickBoardListByPage(@RequestParam(value = "page",required = false, defaultValue = "1") int page){

        return ResponseService.getListResult(kickBoardService.getKickBoardListByPage(page), true);
    }

    @ApiOperation(value = "킥보드 정보 수정 /관리자")
    @PutMapping("/name-price-pos-memo-status/kick-board-id/{kickBoardId}")
    public CommonResult putNamePricePosXYMemoIsUse(@PathVariable long kickBoardId, @RequestParam("status") KickBoardStatus status, @RequestBody @Valid KickBoardNamePriceXYMemoIsUseUpdateRequest updateRequest){
        kickBoardService.putNamePricePosXYMemoIsUse(kickBoardId, status, updateRequest);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "킥보드 id로 정보 불러오기 /회원 및 관리자")
    @GetMapping("/info/kick-board-id/{kickBoardId}")
    public SingleResult<KickBoardResponse> getKickBoardSingleData(@PathVariable long kickBoardId){
        return ResponseService.getSingleResult(kickBoardService.getKickBoardSingleData(kickBoardId));
    }
}

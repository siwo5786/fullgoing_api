package com.fullgoing.fullgoingapi.controller;

import com.fullgoing.fullgoingapi.entity.Member;
import com.fullgoing.fullgoingapi.entity.RemainingAmount;
import com.fullgoing.fullgoingapi.enums.PassType;
import com.fullgoing.fullgoingapi.model.common.CommonResult;
import com.fullgoing.fullgoingapi.model.remainingAmount.RemainingAmountChargePriceRequest;
import com.fullgoing.fullgoingapi.service.amountChargingHistory.AmountChargingHistoryService;
import com.fullgoing.fullgoingapi.service.common.ResponseService;
import com.fullgoing.fullgoingapi.service.member.MemberDataService;
import com.fullgoing.fullgoingapi.service.member.ProfileService;
import com.fullgoing.fullgoingapi.service.remainingAmount.RemainingAmountService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "잔여 금액 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/remaining-amount")

public class RemainingAmountController {
    private final RemainingAmountService remainingAmountService;
    private final AmountChargingHistoryService amountChargingHistoryService;
    private final MemberDataService memberDataService;
    private final ProfileService profileService;

    @ApiOperation(value = "금액 충전 /회원")
    @PutMapping("/my/plus/price")
    public CommonResult plusMyPrice(@RequestBody @Valid RemainingAmountChargePriceRequest request){
        Member member = profileService.getMemberData();
        RemainingAmount remainingAmount = remainingAmountService.getDataByMember(member);
        amountChargingHistoryService.useMileageForChargePrice(member, request.getPrice(), remainingAmount.getRemainMileage());
        remainingAmountService.chargePrice(member,request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "패스 충전 /회원")
    @PutMapping("/my/plus/pass")
    public CommonResult plusMyPass(@RequestParam("pass-type") PassType passType){
        Member member = profileService.getMemberData();
        remainingAmountService.chargePass(member,passType);
        amountChargingHistoryService.plusPass(member, passType);

        return ResponseService.getSuccessResult();
    }
    @ApiOperation(value = "금액 충전 /관리자")
    @PutMapping("/plus/price/member-id/{memberId}")
    public CommonResult plusPrice(@PathVariable long memberId, @RequestBody @Valid RemainingAmountChargePriceRequest request){
        Member member = memberDataService.getMemberById(memberId);
        amountChargingHistoryService.chargePriceByAdmin(member, request.getPrice());
        remainingAmountService.chargePriceByAdmin(member,request);

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "패스 충전 /관리자")
    @PutMapping("/plus/pass/member-id/{memberId}")
    public CommonResult plusPass(@PathVariable long memberId, @RequestParam("pass-type") PassType passType){
        Member member = memberDataService.getMemberById(memberId);
        remainingAmountService.chargePassByAdmin(member,passType);
        amountChargingHistoryService.chargePassByAdmin(member, passType);

        return ResponseService.getSuccessResult();
    }

}

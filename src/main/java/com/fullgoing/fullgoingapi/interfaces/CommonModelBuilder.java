package com.fullgoing.fullgoingapi.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
